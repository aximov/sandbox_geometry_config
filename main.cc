#include <iostream>
#include <memory>
#include "LoadGeometryConfig.hh"
#include "GeometryConfig.hh"

int main() {
  auto mLoadGeometryConfig =
      std::make_unique<LoadGeometryConfig<GeometryConfig>>
          ("/Users/akimoto/workspace/sandbox_geometry_config/data.json");
  if (mLoadGeometryConfig->isLoaded()) std::cout << "Loaded!" << std::endl;

  if (mLoadGeometryConfig) std::cout << "mL has the ownership" << std::endl;
  else std::cout << "mL doesn't have the ownership." << std::endl;

  mLoadGeometryConfig->print_all();

  return 0;
}