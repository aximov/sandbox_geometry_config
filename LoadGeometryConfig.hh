//
// Created by Akimoto Daichi on 2018-12-20.
//
// Anti-corruption layer (translator).
// `loadGeometryConfigFile()` takes the responsibility to build a geometry config instance.
// Note: `loadGeometryConfigFile()` could read two or more parameterization,
//       but it reads only the first parameterization coming in for now,
//       because of the fact that resonantor uses one parameterization set only at a run.

#ifndef SANDBOX_GEOMETRY_CONFIG_LOADGEOMETRYCONFIG_H
#define SANDBOX_GEOMETRY_CONFIG_LOADGEOMETRYCONFIG_H


#include <fstream>
#include <string>
#include <vector>
#include <memory>
#include <cassert>
#include <iostream>
#include <iterator>
#include <sstream>
#include "picojson.h"

template<class geometry_config_t>
class LoadGeometryConfig {
public:
  LoadGeometryConfig();

  explicit LoadGeometryConfig(const std::string &filename);

  virtual ~LoadGeometryConfig();

  bool isLoaded();

  void print_all();

private:
  void loadGeometryConfigFile(const std::string &filename);

  geometry_config_t loadedGeometryConfig;
};

#include <cassert>
#include <fstream>
#include <fstream>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include "picojson.h"

template<class geometry_config_t>
LoadGeometryConfig<geometry_config_t>::LoadGeometryConfig() = default;

template<class geometry_config_t>
LoadGeometryConfig<geometry_config_t>::LoadGeometryConfig(const std::string &filename) {
  loadGeometryConfigFile(filename);
}

template<class geometry_config_t>
LoadGeometryConfig<geometry_config_t>::~LoadGeometryConfig() = default;

template<class geometry_config_t>
bool LoadGeometryConfig<geometry_config_t>::isLoaded() {
  return !loadedGeometryConfig.empty();
}

template<class geometry_config_t>
void LoadGeometryConfig<geometry_config_t>::loadGeometryConfigFile(const std::string &filename) {

  std::ifstream fs;
  fs.open(filename, std::ios::binary);
  assert(fs);
  picojson::value val;
  fs >> val;
  fs.close();

  const auto &obj = val.get<picojson::object>();
  const auto &tables = obj.at("tables").get<picojson::array>();

  const auto &parameterize = tables[0];
  const auto &param_obj = parameterize.get<picojson::object>();
  const auto &parameterize_method = param_obj.at("parameterize_method").get<std::string>();
  loadedGeometryConfig.set_parameterize_method(parameterize_method);

  const auto &table = param_obj.at("table").get<picojson::array>();
  for (const auto &column : table) {
    const auto &column_obj = column.get<picojson::object>();
    const auto &quantity = column_obj.at("quantity").get<std::string>();
    const auto &unit = column_obj.at("unit").get<std::string>();

    std::vector<double> rows;
    for (const auto &row : column_obj.at("rows").get<picojson::array>()) {
      rows.push_back(row.get<double>());
    }
    loadedGeometryConfig.add_quantity(quantity, unit, rows);
  }
}

template<class geometry_config_t>
void LoadGeometryConfig<geometry_config_t>::print_all() {
  std::cout << loadedGeometryConfig.parameterize_method() << std::endl;
  for (const auto &q : loadedGeometryConfig.quantity()) {
    std::cout << q << std::endl;
    std::cout << loadedGeometryConfig.unit_of(q) << std::endl;
    for (const auto &v: loadedGeometryConfig.values_of(q)) {
      std::cout << v << std::endl;
    }
  }
}

#endif //SANDBOX_GEOMETRY_CONFIG_LOADGEOMETRYCONFIG_H