//
// Created by Akimoto Daichi on 2018-12-20.
//

#ifndef SANDBOX_GEOMETRY_CONFIG_GEOMETRYCONFIG_H
#define SANDBOX_GEOMETRY_CONFIG_GEOMETRYCONFIG_H


#include <string>
#include <vector>
#include <unordered_map>

class GeometryConfig {
public:
  GeometryConfig();

  explicit GeometryConfig(std::string parameterize_method);
  virtual ~GeometryConfig();
  std::string parameterize_method() const;

  std::vector<std::string> quantity() const;

  std::string unit_of(std::string quantity) const;

  std::vector<double> values_of(std::string quantity) const;

  bool empty();

  void add_quantity(std::string quantity, std::string unit, std::vector<double> values);

  void set_parameterize_method(std::string parameterize_method_name);

private:
  std::string parameterize_method_;
  std::vector<std::string> quantity_;
  std::unordered_map<std::string, std::string> unit_of_; // of quantity
  std::unordered_map<std::string, std::vector<double>> values_of_; // of quantity
};


#endif //SANDBOX_GEOMETRY_CONFIG_GEOMETRYCONFIG_H
