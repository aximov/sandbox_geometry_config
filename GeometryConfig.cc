//
// Created by Akimoto Daichi on 2018-12-20.
//

#include "GeometryConfig.hh"

GeometryConfig::GeometryConfig() = default;

GeometryConfig::GeometryConfig(std::string parameterize_method)
    : parameterize_method_(move(parameterize_method)) {
}

GeometryConfig::~GeometryConfig() = default;

std::string GeometryConfig::parameterize_method() const { return parameterize_method_; }

bool GeometryConfig::empty() {
  return parameterize_method_.empty();
}

std::vector<std::string> GeometryConfig::quantity() const {
  return quantity_;
}

std::string GeometryConfig::unit_of(std::string quantity) const {
  return unit_of_.at(quantity);
}

std::vector<double> GeometryConfig::values_of(std::string quantity) const {
  return values_of_.at(quantity);
}

void GeometryConfig::add_quantity(std::string quantity, std::string unit, std::vector<double> values) {
  quantity_.push_back(quantity);
  unit_of_[quantity] = std::move(unit);
  values_of_[quantity] = std::move(values);
}

void GeometryConfig::set_parameterize_method(std::string parameterize_method_name) {
  parameterize_method_ = std::move(parameterize_method_name);
}
